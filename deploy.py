import sys
sys.dont_write_bytecode = True

import subprocess
import os
import shutil
from cmd import Cmd
import platform

import deploy_config


class DeployPrompt(Cmd):

    def __init__(self):
        Cmd.__init__(self)

        self.plat = platform.system()
        print()

        self.VERSION = 'v0.6'
        self.prompt = '(DEPLOY)>> '
        self.intro = 'Deploy tool ' + self.VERSION + ' Running on ' + self.plat

        self.LOG_ERR = '[ERROR] - {}'
        self.LOG_WARN = '[WARN] - {}'
        self.LOG_INFO = '[INFO] - {}'

        self.plat_setting = {
            'Windows': {},
            'Darwin': {},
            'Linux': {},
        }

        self.plat_deploy = {
            'Windows': self.deploy_win,
            'Darwin': self.deploy_mac,
            'Linux': self.deploy_linux,
        }

        self.plat_find = {
            'Windows': self.find_win,
            'Darwin': self.find_mac,
            'Linux': self.find_linux,
        }

        self.plat_find_mount = {
            'Windows': self.find_mount_win,
            'Darwin': self.find_mount_mac,
            'Linux': self.find_mount_linux,
        }

        self.target_paths_mac = (
            '/Volumes/',  # Mac
            '/media/parallels/',  # Parallels
            '/media/programbox/',  # Virtual Box
        )

        self.target_paths_linux = (
            '',
        )

        self.target_list = []
        self.mountable_list = []
        self.target = None
        self.mount_disk = None

    def do_find(self, args):
        """
        CMD - Find list and store available targets
        Options:
            'mtn' - manage mounting and unmounting of device
        """
        if args == '':
            self.plat_find[self.plat](args)
        elif args == 'mnt':
            self.plat_find_mount[self.plat](args)

    def do_exit(self, args):
        """
        CMD - exit deployment prompt
        """
        print('Exiting deploy prompt')
        print()
        return True

    def do_set(self, args):
        """
        CMD - Set target based on listed targets.
        """
        print()
        if len(self.target_list) > 0:
            the_arg = None
            if args == "":
                print('No argyment using default [0]')
                print()
                the_arg = 0
            else:
                the_arg = int(args)

            try:
                self.target = self.target_list[the_arg]
                print('Target now: {}'.format(self.target))
            except IndexError:
                print('ERROR - Argument not valid')
            except ValueError:
                print('ERROR - Argument not valid')
        else:
            print('ERROR - Target(s) not found, please use "find" to search for targets')

    def do_findset(self, args):
        self.do_find(args)
        self.do_set('0')

    def do_deploy(self, args):
        """
        CMD - Call deploy function based on platform
        """
        print()
        self.plat_deploy[self.plat](args)

    def do_clean(self, args):
        if os.path.exists('./deploy/'):
            print('Deploy folder exist')
            if args == 'clean':
                print('Clean deploy folder')
                shutil.rmtree('./deploy/')
                os.makedirs('deploy')
        else:
            os.makedirs('deploy')

    def find_win(self, args):
        print()

        find_disks = subprocess.check_output(['wmic', 'logicaldisk', 'get', 'name', ',', 'volumename']).decode().split()
        self.target_list = []

        for count, item in enumerate(find_disks):
            for target in deploy_config.targets:
                if item == target:
                    # eval_path = find_disks[coucountnter-1] + find_disks[count]
                    self.target_list.append(find_disks[count-1])
                    # print('({}) - {}'.format(count, eval_path))

        # Show selection options in prompt
        self.find_disp_target_options()

    def find_mac(self, args):
        print()

        self.target_list = []

        for path in self.target_paths_mac:
            for target in deploy_config.targets:
                eval_path = path + target
                if os.path.exists(eval_path):
                    self.target_list.append(eval_path)

        # Show selection options in prompt
        self.find_disp_target_options()

    def find_linux(self, args):
        pass

    def find_disp_target_options(self):
        if len(self.target_list) > 0:
            for count, target in enumerate(self.target_list):
                print('({}) - {}'.format(count, target))
        else:
            print('No targets found')

    def find_disp_disk_options(self):
        if len(self.mountable_list) > 0:
            for count, disk in enumerate(self.mountable_list):
                print('({}) - {}'.format(count, disk))
        else:
            print('No targets found')

    def find_mount_win(self, args):
        pass

    def find_mount_mac(self, args):
        print()

        output = None
        self.mountable_list = []

        for disk in range(2, 6):
            the_disk = 'disk{}'.format(str(disk))
            try:
                output = subprocess.check_output(['diskutil', 'list', the_disk]).decode().split()
            except subprocess.CalledProcessError:
                output = None
                # print('No {} found'.format(the_disk))

            if output is not None:
                # Only evaluate mountable drive if it is external
                if output[1] == '(external,' and 'DOS_FAT_12' in output:
                    print('{} is external and formatted to FAT12'.format(the_disk))
                    # Check to see if the disk is on the targets list
                    for target in deploy_config.targets:
                        if output[(output.index('DOS_FAT_12') + 1)] == target:
                            self.mountable_list.append((the_disk, target))

        # Show selection options in prompt
        self.find_disp_disk_options()

    def find_mount_linux(self, args):
        pass

    def deploy_win(self, args):
        if self.target is not None:

            if os.path.exists('./deploy/'):
                print('Deploy folder exist')
            else:
                os.makedirs('deploy')

            for file in deploy_config.files:
                print(file[0])
                if file[1] == 'std':
                    shutil.copy(file[0], './deploy/')
                elif file[1] == 'min':
                    self.minify_deploy(file[0])

            print('Copy files to device')
            if deploy_config.robocopy_purge:
                pass
                # subprocess.call(['robocopy', './deploy', self.target])
                # robocopy purge for rsync delete like functionality
            else:
                # shutil.copytree('./deploy/', self.target)
                subprocess.call(['robocopy', './deploy', self.target])

        else:
            print('Target invalid')

    def deploy_mac(self, args):
        if self.target is not None:

            if os.path.exists('./deploy/'):
                print('Deploy folder exist')
            else:
                os.makedirs('deploy')

            print('')
            print('Prep files for deploy')
            for file in deploy_config.files_sync:
                print(file[0])
                if file[1] == 'std':
                    shutil.copy(file[0], './deploy/')
                elif file[1] == 'min':
                    self.minify_deploy(file[0])

            print('')
            print('Copy files to device')
            if deploy_config.rsync_delete:
                print('rsync with delete')
                subprocess.call(['rsync', '-a', '--delete', '--inplace', './deploy/', self.target])
            else:
                print('rsync')
                subprocess.call(['rsync', '-a', '--inplace', './deploy/', self.target])
        else:
            print('Target invalid')

    def deploy_linux(self, args):
        pass

    def minify_deploy(self, filename):
        if filename[-3:] == '.py':
            subprocess.call(['pyminifier', '-o', './deploy/' + filename, filename])
        else:
            print(self.LOG_WARN.format('Not a py file, cannot minify'))
            shutil.copy(filename, './deploy/')


if __name__ == '__main__':

    prompt = DeployPrompt()
    prompt.cmdloop()
