# MP_deploy

Deployment tool for USB based Micropython devices

Mac and Unis use rsync

Windows uses robocopy

## Config file

Example config file

std = Standard file
min = minify (for python files only)

```python
# Files to be uploaded with their upload update.
# 'std' = Standard copy
# 'min' = Minify then copy of Python files.

# Files intended for full build
files_deploy = (
    ('boot.py', 'std'),
    ('main.py', 'std'),
)

# Select files for partial build often used for active development
files_develop = (
    ('boot.py', 'std'),
    ('main.py', 'std'),
)

# Target names (Name of removable device)
targets = {
    'PYBFLASH',
    'MP405',
    'BRAD405',
}

# Sync folder using unix rsync and deletes files not in the source folder
rsync_delete = False

# UNTESTED sync folder using Windows robocopy and delete files not in the source folder
robocopy_purge = False
```

## Find available targets

```shell
(DEPLOY)>> find

(0) - /Volumes/PYBFLASH
```

## Set target

```shell
(DEPLOY)>> set 0

Target now: /Volumes/PYBFLASH
```

## Find and set first target found

Used when only one know target is connected for quicker deployments.

```shell
(DEPLOY)>> findset

(0) - /Volumes/PYBFLASH

Target now: /Volumes/PYBFLASH
```

## Deploy to Target

```shell
(DEPLOY)>> deploy

Deploy folder exist
boot.py
main.py
main.py (19) reduced to 84 bytes (442.11% of original size)
Copy files to device
```

