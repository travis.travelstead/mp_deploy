# Files to be uploaded with their upload update.
# 'std' = Standard copy
# 'min' = Minify then copy of Python files.

# Files intended for full build
files_deploy = (
    'boot.py', 
    'main.py',
)

# Select files for partial build often used for active development
files_develop = (
    'boot.py',
    'main.py',
)

# Files for sync deployment, use 'std' for standard files, or 'min' for minified files
files_sync = (
    ('boot.py', 'std'),
    ('main.py', 'std'),
)

# Target names (Name of removable device)
targets = {
    'PYBFLASH',
}

# Sync folder using unix rsync and deletes files not in the source folder
rsync_delete = False

# UNTESTED sync folder using Windows robocopy and delete files not in the source folder
robocopy_purge = False